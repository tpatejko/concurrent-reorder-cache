#pragma once

#include "kernel.hpp"
#include <random>
#include <algorithm>
#include <numeric>
#include <chrono>

auto generate_random_tests(
			   int no_tests,
			   const std::vector<size_t>& format_hashes,
			   const std::vector<reorder_type>& reorder_types)
  -> std::vector<std::pair<size_t, reorder_type>>
{
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<size_t> dist_formats(0, format_hashes.size()-1);
  std::uniform_int_distribution<size_t> dist_reorders(0, reorder_types.size()-1);

  std::vector<std::pair<size_t, reorder_type>> tests;

  for (auto i = 0; i < no_tests; i++) {
    auto fi = dist_formats(mt);
    auto ri = dist_reorders(mt);

    tests.push_back({ format_hashes[fi], reorder_types[ri] });
  }

  return tests;
}

auto generate_equal_writes_reads(int no_tests,
                                 const std::vector<reorder_type>& reorder_types)
  -> std::vector<std::pair<size_t, reorder_type>>
{
  std::vector<std::pair<size_t, reorder_type>> tests;
  auto no_keys = no_tests / reorder_types.size() / 2;
  std::vector<size_t> keys(no_keys);

  std::iota(std::begin(keys), std::end(keys), 0);

  for (auto k : keys) {
    for (auto r : reorder_types) {
      tests.push_back({ k, r });
    }
    for (auto r : reorder_types) {
      tests.push_back({ k, r });
    }
  }

  return tests;
}

auto generate_equal_writes_reads_v2(int no_tests,
                                    const std::vector<reorder_type>& reorder_types)
  -> std::vector<std::pair<size_t, reorder_type>>
{
  auto no_accesses = 4;
  auto no_keys = no_tests / reorder_types.size() / no_accesses;
  std::vector<size_t> keys(no_keys);

  std::iota(std::begin(keys), std::end(keys), 0);

  std::vector<std::pair<size_t, reorder_type>> tests;

  for (auto k : keys) {
    for (auto r : reorder_types) {
      for (auto i = 0; i < no_accesses; i++) {
        tests.push_back({ k, r });
      }
    }
  }

  return tests;
}

auto generate_tests(
                    int no_tests,
                    const std::vector<size_t>& format_hashes,
                    const std::vector<reorder_type>& reorder_types)
  -> std::vector<std::pair<size_t, reorder_type>>
{
  auto size_reorder_types = reorder_types.size();
  auto size_format_hashes = format_hashes.size();

  auto permute = [](std::vector<size_t> format_hashes) -> std::vector<size_t>
    {
     std::vector<size_t> perms;

     do {
       std::copy(std::begin(format_hashes), std::end(format_hashes), std::back_inserter(perms));
     } while (std::next_permutation(std::begin(format_hashes), std::end(format_hashes)));

     return perms;
    };

  auto broadcast_reorder_types = [](const std::vector<reorder_type>& elements, int times)
    -> std::vector<reorder_type>
    {
     std::vector<reorder_type> broadcast_elements(times * elements.size());

     std::accumulate(std::begin(elements), std::end(elements), std::begin(broadcast_elements),
                     [times](auto it, auto e) {
                       std::fill(it, std::next(it, times), e);
                       return std::next(it, times);
                     });

     return broadcast_elements;
    };

  auto broadcast_vector = [](const std::vector<size_t>& v, int times) -> std::vector<size_t>
    {
     std::vector<size_t> broadcast_elements;

     for (auto i = 0; i < times; i++) {
       std::copy(std::begin(v), std::end(v), std::back_inserter(broadcast_elements));
     }

     return broadcast_elements;
    };

  auto perms_format_hashes = permute(format_hashes);
  auto bc_reorder_types = broadcast_reorder_types(reorder_types, perms_format_hashes.size());
  auto bc_perm_format_hashes = broadcast_vector(perms_format_hashes, size_reorder_types);

  std::vector<std::pair<size_t, reorder_type>> perms;

  for (auto i = 0; i < bc_reorder_types.size(); i++) {
    perms.push_back(std::make_pair(bc_perm_format_hashes[i], bc_reorder_types[i]));
  }

  int times = no_tests / perms.size();

  size_t tests_size = perms.size() * times;
  size_t tail = no_tests - tests_size;

  std::vector<std::pair<size_t, reorder_type>> tests;

  for (auto i = 0; i < times; i++) {
    std::copy(std::begin(perms), std::end(perms), std::back_inserter(tests));
  }

  std::copy(std::begin(perms), std::begin(perms) + tail, std::back_inserter(tests));

  std::random_device rd;
  std::mt19937 g(rd());

  std::shuffle(std::begin(tests), std::end(tests), g);

  return tests;
}

struct sleep_time_t {
  std::random_device rd;
  std::mt19937 mt;
  std::uniform_int_distribution<uint64_t> dist;

  sleep_time_t() : rd(), mt(rd()), dist(50, 400) {}

  uint64_t operator()() {
    return dist(mt);
  }
};

std::chrono::duration<uint64_t, std::milli> get_sleep_time() {
  static sleep_time_t sleep_time;

  std::chrono::duration<uint64_t, std::milli> d(sleep_time());

  return d;
}
