#pragma once

#include <thread>
#include <mutex>
#include <memory>
#include <vector>
#include <deque>
#include <condition_variable>
#include <functional>
#include <atomic>

class join_threads {
  std::vector<std::thread>& threads;

public:
  explicit join_threads(std::vector<std::thread>& threads_)
    : threads(threads_)
  {}

  ~join_threads() {
    for (auto i = 0; i < threads.size(); i++) {
      if (threads[i].joinable()) {
        threads[i].join();
      }
    }
  }
};

struct latch {
  std::atomic<int> counter;

  latch(int c) : counter(c) {}

  void count_down() {
    counter--;
  }

  void wait() {
    int v = 0;
    while(!counter.compare_exchange_strong(v, 0));
  }
};


class thread_pool {
  std::vector<std::thread> threads;
  join_threads joiner;
  latch thread_latch;

  void worker_thread(std::deque<std::function<void()>> work_queue) {
    while (!work_queue.empty()) {
      auto task = work_queue.front();
      work_queue.pop_front();

      task();
    }

    thread_latch.count_down();
  }

public:
  thread_pool(const std::vector<std::deque<std::function<void()>>>& work_queues)
    : joiner(threads)
    , thread_latch(work_queues.size())
  {
    for (auto q : work_queues) {
      threads.push_back(std::thread(&thread_pool::worker_thread, this, std::move(q)));
    }
  }

  void wait() {
    thread_latch.wait();
  }
};

/*
Some old stuff

template<typename T>
class threadsafe_queue {
  struct node {
    std::shared_ptr<T> data;
    std::unique_ptr<node> next;
  };

  std::mutex head_mutex;
  std::unique_ptr<node> head;
  std::mutex tail_mutex;
  node* tail;
  std::condition_variable data_cond;

public:
  threadsafe_queue()
    : head(new node)
    , tail(head.get())
  {}

  threadsafe_queue(const threadsafe_queue&) = delete;
  threadsafe_queue& operator=(const threadsafe_queue&) = delete;

  void push(T new_value) {
    auto new_data = std::make_shared<T>(std::move(new_value));
    std::unique_ptr<node> p(new node);

    {
      std::lock_guard<std::mutex> tail_lock(tail_mutex);
      tail->data = new_data;
      auto new_tail = p.get();
      tail->next = std::move(p);
      tail = new_tail;
    }

    data_cond.notify_one();
  }

private:
  node* get_tail() {
    std::lock_guard<std::mutex> tail_lock(tail_mutex);
    return tail;
  }

  std::unique_ptr<node> pop_head() {
    auto old_head = std::move(head);
    head = std::move(old_head->next);

    return old_head;
  }

  std::unique_ptr<std::mutex> wait_for_data() {
    std::unique_ptr<std::mutex> head_lock(head_mutex);
    data_cond.wait(head_lock, [&]() { return head.get() != get_tail();} );

    return std::move(head_lock);
  }

  std::unique_ptr<node> wait_pop_head() {
    std::unique_ptr<std::mutex> head_lock(wait_for_data());

    return pop_head();
  }

  std::unique_ptr<node> wait_pop_head(T& value) {
    std::unique_ptr<std::mutex> head_lock(wait_for_data());
    value = std::move(*head->data);

    return pop_head();
  }

public:
  std::shared_ptr<T> wait_and_pop() {
    auto old_head = wait_pop_head();
    return old_head->data();
  }

  void wait_and_pop(T& value) {
    auto old_head = wait_pop_head(value);
  }

private:
  std::unique_ptr<node> try_pop_head() {
    std::lock_guard<std::mutex> head_lock(head_mutex);

    if (head.get() == get_tail()) {
      return std::unique_ptr<node>();
    }

    return pop_head();
  }

  std::unique_ptr<node> try_pop_head(T& value) {
    std::lock_guard<std::mutex> head_lock(head_mutex);

    if (head.get() == get_tail()) {
      return std::unique_ptr<node>();
    }

    value = std::move(*head->data);
    return pop_head();
  }

public:
  std::shared_ptr<T> try_pop() {
    std::unique_ptr<node> old_head = try_pop_head();
    return old_head ? old_head->data : std::shared_ptr<T>();
  }

  bool try_pop(T& value) {
    auto old_head = try_pop_head(value);
    return (bool)old_head;
  }

  void empty() {
    std::lock_guard<std::mutex> head_lock(head_mutex);
    return (head.get() == get_tail());
  }
};

class thread_pool {
  std::atomic_bool done;
  threadsafe_queue<std::function<void()>> work_queue;

  std::vector<std::thread> threads;

  join_threads joiner;

  void worker_thread() {
    while (!done) {
      std::function<void()> task;

      if (work_queue.try_pop(task)) {
        task();
      } else {
        std::this_thread::yield();
      }
    }
  }

public:
  thread_pool()
    : done(false)
    , joiner(threads) {
    const unsigned thread_count = std::thread::hardware_concurrency();

    try {
      for (auto i = 0; i < thread_count; i++) {
        threads.push_back(std::thread(&thread_pool::worker_thread, this));
      }
    } catch (...) {
      done = true;
      throw;
    }
  }

  ~thread_pool() {
    done = true;
  }

  template<typename FT>
  void submit(FT f) {
    work_queue.push(std::function<void()>(f));
  }
};
*/
