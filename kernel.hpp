#pragma once

#include "gemm.h"

using reorder_type = int;

struct kernel_t {
  //  static std::atomic<int> counter;
  using saxpy_func_t = void (*)(const float*, const float*, float*, int, int);
  reorder_type reorder;
  std::unique_ptr<saxpyFunc> saxpy_code;
  saxpy_func_t saxpy_func;

  int m;
  int n;
  std::unique_ptr<float[]> a;
  std::unique_ptr<float[]> x;
  std::unique_ptr<float[]> y;

  kernel_t(reorder_type r, int m, int n)
    : reorder(r)
    , saxpy_code(new saxpyFunc())
    , saxpy_func(reinterpret_cast<saxpy_func_t>(saxpy_code->getCode()))
    , m(m)
    , n(n)
    , a(std::make_unique<float[]>(m*n))
    , x(std::make_unique<float[]>(n))
    , y(std::make_unique<float[]>(m))
  {
    //    kernel_t::counter++;
  }

  void operator()() {
    for(int i = 0; i < m*n; ++i) {
      a[i] = i;
    }

    for(int i = 0; i < n; ++i) {
      x[i] = 2.0f*i;
    }

    saxpy_func(a.get(), x.get(), y.get(), m, n);
  }
};

//std::atomic<int> kernel_t::counter = 0;
