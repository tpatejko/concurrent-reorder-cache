#include "test.hpp"

#include <unordered_map>
#include <memory>
#include <future>
#include <shared_mutex>

template<typename K, typename V, template<typename, typename, typename...> class map_t = std::unordered_map>
struct cache_mt_t : map_t<K, V> {
  std::shared_mutex m;
};

struct cache_t {
  std::shared_mutex m;

  using format_key_t = size_t;
  using reorder_key_t = reorder_type;

  using cache_value_t = std::shared_ptr<kernel_t>;
  using value_t = std::shared_future<cache_value_t>;

  using reorder_cache_t = cache_mt_t<reorder_key_t, value_t>;
  using format_cache_t = cache_mt_t<format_key_t, std::unique_ptr<reorder_cache_t>>;

  format_cache_t cache;

  value_t add(reorder_cache_t& cache,
              reorder_key_t key,
              const value_t& value)
  {
    std::unique_lock ul(cache.m);
    auto it = cache.insert({ key, value });

    if (it.second) {
      return value_t();
    } else {
      return it.first->second;
    }
  }

  value_t get_or_add(reorder_cache_t& cache,
                     reorder_key_t reorder_key,
                     const value_t& value)
  {
    std::shared_lock sl(cache.m);
    // check if there is already cached kernel with given reorder key
    auto jt = cache.find(reorder_key);

    if (jt == std::end(cache)) {
      sl.unlock();
      return add(cache, reorder_key, value);
    } else {
      return jt->second;
    }
  }

  value_t get_or_add(format_key_t format_key,
                     reorder_key_t reorder_key,
                     const value_t& value)
  {
    std::shared_lock<std::shared_mutex> sl(m);

    auto it = cache.find(format_key);

    if (it == std::end(cache)) {
      // this means that there is no reorder cache attached to the format key
      std::unique_ptr<reorder_cache_t> c(new reorder_cache_t());

      sl.unlock();
      {
        std::unique_lock<std::shared_mutex> ul(m);
        auto ri = cache.insert(std::make_pair(format_key, std::move(c)));
        auto& reorder_cache = *(ri.first->second);
        ul.unlock();
        return get_or_add(reorder_cache, reorder_key, value);
      }
    } else {
      // there is reorder cache attached to the format key
      // try to find kernel of a given type reorder_key
      auto& reorder_cache = *(it->second);
      sl.unlock();
      return get_or_add(reorder_cache, reorder_key, value);
    }
  }

  void clear(){
    cache.clear();
  }

  void dump() {
    for (auto& f : cache) {
      std::cout << "format: " << f.first << "\n";
      for (auto& rc: *(f.second)) {
        std::cout << "->" << rc.first << " " << rc.second.get()->y.get() << "\n";
      }
    }
  }

  void dump_sizes() {
    std::cout << "Total size: " << cache.size() << "\n";

    for (auto& f : cache) {
      std::cout << "-> reorder key size " << f.first << " " << f.second->size() << "\n";
    }
  }
};

cache_t& get_reorder_cache() {
  static cache_t cache;
  return cache;
}
