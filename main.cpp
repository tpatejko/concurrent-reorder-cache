#include "thread_utils.hpp"
#include "cache.hpp"

#include <map>
#include <iostream>
#include <chrono>
#include <algorithm>

enum result_t {
  miss,
  hit,
};

struct reorder_primitive {
  std::shared_ptr<kernel_t> kernel;

  result_t init(size_t key, reorder_type type) {
    auto& cache = get_reorder_cache();
    std::promise<cache_t::cache_value_t> p;

    auto ker = cache.get_or_add(key, type, p.get_future());

    if (ker.valid()) {
      // just like in the primitive cache: when future is valid it's present in the cache
      // or it's being created by another thread.
      kernel = ker.get();
      return hit;
    } else {
      kernel = std::make_shared<kernel_t>(type, 256, 256);
      p.set_value(kernel);
      return miss;
    }
  }

  void execute() {
    (*kernel)();
  }
};

struct results_t {
  size_t key;
  reorder_type reorder;
  size_t thread_id;
  result_t result;
};

void func(std::vector<results_t>& results, size_t idx, size_t key, reorder_type type) {
  reorder_primitive p;
  auto res = p.init(key, type);
  p.execute();

  auto& r = results[idx];
  r.key = key;
  r.reorder = type;
  r.thread_id = std::hash<std::thread::id>{}(std::this_thread::get_id());
  r.result = res;
}

struct validation_results_t {
  int misses;
  int hits;
};

using validation_map_t = std::map<std::pair<size_t, reorder_type>, validation_results_t>;

validation_map_t count_queries(const std::vector<results_t>& results) {
  validation_map_t validation_map;
  for (auto r : results) {
    auto p = std::make_pair(r.key, r.reorder);
    auto it = validation_map.find(p);

    if (it == std::end(validation_map)) {
      validation_results_t vr;
      vr.misses = 0;
      vr.hits = 0;
      auto jt = validation_map.insert({p, vr});
      it = jt.first;
    }

    if (r.result == miss) {
      it->second.misses++;
    } else {
      it->second.hits++;
    }
  }

  return validation_map;
}

int main() {

  std::vector<size_t> format_hashes(250);
  std::iota(std::begin(format_hashes), std::end(format_hashes), 0);

  std::vector<reorder_type> reorder_types(32);
  std::iota(std::begin(reorder_types), std::end(reorder_types), 0);

  auto tests = generate_random_tests(1000, format_hashes, reorder_types);
  //auto tests = generate_equal_writes_reads_v2(320000, reorder_types);

  std::vector<results_t> par_results{tests.size()};
  std::vector<results_t> seq_results{tests.size()};

  {
    auto no_work_queues = std::thread::hardware_concurrency();
    std::vector<std::deque<std::function<void()>>> work_queues(no_work_queues);

    for (auto i = 0; i < tests.size(); i++) {
      auto idx = i % no_work_queues;
      work_queues[idx].push_back([&results=par_results, idx=i, key=tests[i].first, type=tests[i].second]()
                               { func(results, idx, key, type); });
    }

    thread_pool pool(work_queues);

    pool.wait();
    //    using namespace std::chrono_literals;
    //std::this_thread::sleep_for(10s);
  }

  {
    get_reorder_cache().clear();
    for (size_t i = 0; i < tests.size(); i++) {
      func(seq_results, i, tests[i].first, tests[i].second);
    }
  }

  auto par_val_map = count_queries(par_results);
  auto seq_val_map = count_queries(seq_results);

  for (auto kv : seq_val_map) {
    auto par_v = par_val_map[kv.first];

    if ((par_v.misses != kv.second.misses) || (par_v.hits != kv.second.hits)) {
      std::cout << "Incorrect -> ";
      std::cout << "Key " << kv.first.first << " reorder " << kv.first.second
                << " par misses " << par_v.misses << " par hits " << par_v.hits << " vs "
                << " seq misses " << kv.second.misses << " seq hits " << kv.second.hits << "\n";

    } else {
      //      std::cout << "Key " << kv.first.first << " reorder " << kv.first.second
      //        << " misses " << par_v.misses << " hits " << par_v.hits << "\n";
    }
  }
  return 0;
}
