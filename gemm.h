#include "xbyak/xbyak.h"
#include "xbyak/xbyak_util.h"

// tpatejko: saxpy implementation by Jacek Czaja
void saxpy(const float *A,const float *x, float *y, int m, int n);

// We can assume that matrix dimensions are divisible by 8
struct saxpyFunc : public Xbyak::CodeGenerator {
  saxpyFunc() {
// calling convenrtion RDI, RSI, RDX, RCX, R8, R9
// XMM0-7 (ints are passed that way)
//      RDI - PTR A
//      RSI - PTR X
//      RDX - PTR Y
//      RCX - M
//      R8  - N

// Regsters that need to be preserved: RBX,RBP, R12-R15

  shl (rcx,2);  // num of Output elements * size of float (4)

  push(rbx);

  xor_(rax,rax);
  xor_(r9,r9); // i
  xor_(r10,r10); // j

  L("for_j");
    vxorps(ymm3,ymm3);  // 8 partial sums of multiplication
    xor_(rbx,rbx);
    L("for_i");
      vmovups(ymm1,ptr [rdi + rax]);  // A
      vmovups(ymm0,ptr [rsi + rbx*4]);  // X
      vmulps(ymm2,ymm1,ymm0);
      vaddps(ymm3,ymm2);
      add(rax,32);  // next 8 elements
      add(rbx,8);
      cmp(rbx,r8);
      jnz("for_i");

    // Here We sum 8 partial sums and add it to Y[j]
    vextractf128(xmm0,ymm3,1);  // Get upper half (4 floats)
    vaddps(xmm0,xmm0,xmm3);     // and add it to lower 4 floats
    vhaddps(xmm0,xmm0,xmm0);    // make two sums (first two floats together and third and forth floats together)
    vhaddps(xmm0,xmm0,xmm0);    // Final sum
    vaddss(xmm0,ptr[rdx + r10]); // Y[.] + A[.]*X[.]
    vmovss(ptr[rdx + r10],xmm0); // Y[.] <- Y[.] + A[.]*X[.]

    lea (r9,ptr [r9+r8*4]); // jump to next raw
    mov(rax,r9);

    add(r10,4);
    cmp(r10,rcx);
    jnz("for_j");

  pop(rbx);

  ret();
}
};
